mod compress;
use compress::lzw_compress as compress;

use crate::config::Config;
use crate::{BLACK_INDEX, WHITE_INDEX};

/// Creates an image with the dimensions `config.width`x`config.height` based
/// on the parameter `i` that is used to generate images in a sequence in such
/// a way that consecutive values of i result in consecutive images.
///
/// These images repeat [config.period]ically.
/// This means that `create_image(n) == create_image(n + config.period)`.
/// You should use this behavior to reuse the same output multiple times since
/// this function on its own does not cache the results.
///
/// Returns the compressed image.
pub fn create_image(config: &Config, i: usize) -> Vec<u8> {
	// The total amount of pixels per image.
	let pixel_count = config.width as usize * config.height as usize;

	// The maximum height for ellipses which is half the height of the image since
	// the height of the ellipses is measured from the middle to the highest point.
	let max_ellipse_height: usize = config.height as usize / 2;

	// Calculates the offset the ellipses will have in their most "stretched"
	// position.
	let max_ellipse_offset: f64 = config.width as f64 / config.ellipse_count as f64;

	let i = i % config.period;
	let angle = i as f64 * (std::f64::consts::TAU / config.period as f64);
	let sin = angle.sin();
	let cos_abs = angle.cos().abs();

	let mut image_color_indices = Vec::with_capacity(pixel_count);
	for _ in 0..pixel_count {
		image_color_indices.push(BLACK_INDEX)
	}

	let half_width = config.width as f64 / 2.;
	let y = config.height as usize / 2;

	for ellipse in 0..config.ellipse_count {
		let x = (half_width + sin * (half_width - max_ellipse_offset * ellipse as f64))
			as usize;
		let center = Point { x, y };

		let height =
			ellipse as f64 / config.ellipse_count as f64 * max_ellipse_height as f64;
		let width = height * cos_abs;

		draw_ellipse(config, center, width, height, &mut image_color_indices);
	}

	compress(&image_color_indices)
}

/// A point in 2-dimensional space.
#[derive(Copy, Clone)]
pub struct Point {
	x: usize,
	y: usize,
}

impl Point {
	/// Calculates the distance between two points.
	pub fn distance(&self, other: &Point) -> f64 {
		let x_diff_squared = (self.x as f64 - other.x as f64).powf(2.);
		let y_diff_squared = (self.y as f64 - other.y as f64).powf(2.);
		(x_diff_squared + y_diff_squared).sqrt()
	}
}

/// Draws an ellipse around a `center` onto the `image` with a `height` and
/// `width` which are measured from the center to the highest and the rightmost
/// point of the ellipse.
///
/// The drawn circle consists of a [WHITE](crate::WHITE_INDEX) line that is
/// [config.ellipse_thickness](crate::config::config.ellipse_thickness) pixel thick.
#[inline(never)]
pub fn draw_ellipse(
	config: &Config,
	center: Point,
	width: f64,
	height: f64,
	image: &mut [u8],
) {
	let size = height * 2.;
	let linear_eccentricity = (height.powf(2.) - width.powf(2.)).sqrt() as usize;

	let focus_1 = Point {
		x: center.x,
		y: center.y + linear_eccentricity,
	};
	let focus_2 = Point {
		x: center.x,
		y: center.y - linear_eccentricity,
	};

	let half_thickness = config.ellipse_thickness / 2.;
	let half_thickness_u = half_thickness as usize;

	let left_x = center
		.x
		.saturating_sub(width as usize)
		.saturating_sub(half_thickness_u);
	let right_x =
		(center.x + width as usize + half_thickness_u + 1).min(config.width as usize);

	let top_y = center
		.y
		.saturating_sub(height as usize)
		.saturating_sub(half_thickness_u);
	let bottom_y =
		(center.y + height as usize + half_thickness_u + 1).min(config.height as usize);

	for x in left_x..right_x {
		for y in top_y..bottom_y {
			let point = Point { x, y };

			let distance_sum = point.distance(&focus_1) + point.distance(&focus_2);
			let delta = (distance_sum - size).abs();
			if delta < half_thickness {
				let idx = x + y * config.width as usize;
				image[idx] = WHITE_INDEX;
			}
		}
	}
}
