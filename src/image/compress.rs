use crate::LZW_MINIMUM_CODE_SIZE;

/// The maximum codesize of the LZW algorithm used for gif as defined in
/// [gif-spec89a.txt](https://www.w3.org/Graphics/GIF/spec-gif89a.txt).
const MAX_COMPRESSION_CODESIZE: u8 = 12;

/// A table for the LZW compression algorithm, storing known input sequences.
///
/// Use [new_table] for initialisation.
///
/// Use [search_table] to search for a sequence.
///
/// Example:
/// ```
/// let end = 5;
/// let table: Table = new_table(end);
///
/// assert_eq!(search_table(&table, 0), Some(0));
/// assert_eq!(search_table(&table, 15), None);
/// ```
type Table = Vec<Vec<u8>>;

/// Compresses `bytes` using the LZW algorithm and packs the resulting bytes.
///
/// see [Wikipedia](https://en.wikipedia.org/wiki/Lempel–Ziv–Welch) and appendix
/// F of [gif-spec89a.txt](https://www.w3.org/Graphics/GIF/spec-gif89a.txt).
///
/// Example:
/// ```
/// const PIXEL_COUNT: usize = 4
/// const LZW_MINIMUM_CODE_SIZE: u8 = 2
///
/// let compressed = lzw_compress([0, 0, 1, 1]);
///
/// assert_eq!(compressed, [4, 146, 2]);
/// ```
pub fn lzw_compress(bytes: &[u8]) -> Vec<u8> {
	// build up the initial table with color table indices for BLACK and WHITE
	// as well as CLR and END
	const CLR: u8 = 2u8.pow(LZW_MINIMUM_CODE_SIZE as u32);
	const END: u8 = CLR + 1;
	let mut table: Table = new_table(END);

	// A table with the maximum size possible so no further allocation is needed.
	let mut result = Vec::with_capacity((1usize << MAX_COMPRESSION_CODESIZE) - 1);
	let mut cur_input_sequence: Vec<u8> = Vec::new();
	let mut cur_write_byte: u8 = 0;
	let mut offset_in_byte = 0;
	let mut last_found_table_entry: usize = 0;
	// keeping track of the current codesize so we know how many bits to pack
	let mut codesize = LZW_MINIMUM_CODE_SIZE + 1;
	// the maximum index that is possible with the current codesize
	let mut max_idx = (1usize << codesize) - 1;

	le_bit_packing(
		&mut cur_write_byte,
		&mut offset_in_byte,
		codesize,
		CLR as usize,
		&mut result,
	);

	for byte in bytes {
		cur_input_sequence.push(*byte);

		let searched = search_table(&table, &cur_input_sequence, last_found_table_entry);

		// look for the current sequence in table
		// when found, we store the table index in last_found_table_entry
		// otherwise, we have found a new combination
		if let Some(idx) = searched {
			last_found_table_entry = idx;
			continue;
		}

		// add new entry to the table
		let mut new_table_entry = Vec::with_capacity(cur_input_sequence.len());
		for byte in &cur_input_sequence {
			new_table_entry.push(*byte);
		}

		table.push(new_table_entry);

		// update codesize and the maximum index we can reach with it
		if max_idx < table.len() - 2 {
			codesize += 1;
			max_idx <<= 1;
			max_idx |= 1;
		}

		// add last_found_table_entry to the output
		le_bit_packing(
			&mut cur_write_byte,
			&mut offset_in_byte,
			codesize,
			last_found_table_entry,
			&mut result,
		);

		// start buffering again with new byte
		cur_input_sequence.clear();
		cur_input_sequence.push(*byte);
		// We can safely unwrap since the sequence can only be [0] or [1],
		// which are both in the initial table.
		last_found_table_entry = search_table(&table, &cur_input_sequence, 0).unwrap();

		// if the next index would be too big, send CLR
		if codesize >= MAX_COMPRESSION_CODESIZE && table.len() + 1 >= max_idx {
			// signal CLR
			le_bit_packing(
				&mut cur_write_byte,
				&mut offset_in_byte,
				codesize,
				CLR as usize,
				&mut result,
			);

			// clearing the table
			table.truncate(END as usize + 1);

			// resetting codesize and setting max_idx accordingly
			codesize = LZW_MINIMUM_CODE_SIZE + 1;
			max_idx = (1usize << codesize) - 1;
		}
	}

	// add the remaining sequence to the output
	if !cur_input_sequence.is_empty() {
		let idx =
			search_table(&table, &cur_input_sequence, last_found_table_entry).unwrap();

		le_bit_packing(
			&mut cur_write_byte,
			&mut offset_in_byte,
			codesize,
			idx,
			&mut result,
		);
	}

	// signal END
	le_bit_packing(
		&mut cur_write_byte,
		&mut offset_in_byte,
		codesize,
		END as usize,
		&mut result,
	);

	// if there is still something in the `cur_write_byte`, write it out
	if offset_in_byte != 0 {
		result.push(cur_write_byte);
	}

	result
}

/// Initializes a [Table](Table) with `init_size` entries.
fn new_table(init_size: u8) -> Table {
	let mut table: Table = Vec::with_capacity(100);

	for i in 0..=init_size {
		table.push(vec![i]);
	}

	table
}

/// Searches `table` for `cur_input_sequence` and returns the index when found.
///
/// Only searches for indices >= `start_idx` since entries that are based on
/// another entry are always after their base entry.
/// E.g.:
///
/// `idx([0, 0, 1]) < idx([0, 0, 1, 0])`
#[inline(never)]
fn search_table(
	table: &Table,
	cur_input_sequence: &[u8],
	start_idx: usize,
) -> Option<usize> {
	for (table_idx, _) in table.iter().enumerate().skip(start_idx) {
		let entry = &table[table_idx];

		// Apparently rust vec compare does not check for length first on
		// its own.
		// This additional test grants nearly a 50% perf increase on debug.
		if cur_input_sequence.len() == entry.len() && cur_input_sequence == entry {
			return Some(table_idx);
		}
	}

	None
}

/// Packs `table_idx` in `result` in le order
/// under consideration of the `cur_write_byte` and the offset in it.
fn le_bit_packing(
	cur_write_byte: &mut u8,
	offset_in_byte: &mut u8,
	mut len: u8,
	mut table_idx: usize,
	result: &mut Vec<u8>,
) {
	while len > 0 {
		*cur_write_byte |= (table_idx as u8) << *offset_in_byte;

		let free_bits = 8 - *offset_in_byte;
		if len >= free_bits {
			len -= free_bits;
			table_idx >>= free_bits;
			result.push(*cur_write_byte);
			*cur_write_byte = 0;
			*offset_in_byte = 0;
		} else {
			*offset_in_byte += len;
			len = 0;
		}
	}
}
