use crate::HELP_MESSAGE;

#[derive(Debug, Clone)]
#[allow(clippy::enum_variant_names)]
pub enum Error {
	InvalidParam(String),
	ExpectedInt,
	ExpectedFloat,
	ExpectedFileName,
	ParseIntError(std::num::ParseIntError),
	ParseFloatError(std::num::ParseFloatError),
}

#[derive(Clone)]
pub struct Config {
	pub width: u16,
	pub height: u16,
	pub fps: usize,
	pub period: usize,
	pub ellipse_thickness: f64,
	pub ellipse_count: usize,
	pub file_name: String,
	pub thread_count: usize,
}

impl Config {
	pub fn new() -> Result<Config, Error> {
		let mut conf = Config::default();
		let mut args = std::env::args().skip(1);

		while let Some(arg) = args.next() {
			match arg.as_ref() {
				"-x" | "--width" => {
					conf.width = if let Some(arg) = args.next() {
						arg.parse().map_err(Error::ParseIntError)?
					} else {
						return Err(Error::ExpectedInt);
					};
				}
				"-y" | "--height" => {
					conf.height = if let Some(arg) = args.next() {
						arg.parse().map_err(Error::ParseIntError)?
					} else {
						return Err(Error::ExpectedInt);
					};
				}
				"-f" | "--fps" => {
					conf.fps = if let Some(arg) = args.next() {
						arg.parse().map_err(Error::ParseIntError)?
					} else {
						return Err(Error::ExpectedInt);
					};
				}
				"-p" | "--period" => {
					conf.period = if let Some(arg) = args.next() {
						arg.parse().map_err(Error::ParseIntError)?
					} else {
						return Err(Error::ExpectedInt);
					};
				}
				"-t" | "--thickness" => {
					conf.ellipse_thickness = if let Some(arg) = args.next() {
						arg.parse().map_err(Error::ParseFloatError)?
					} else {
						return Err(Error::ExpectedFloat);
					};
				}
				"-c" | "--count" => {
					conf.ellipse_count = if let Some(arg) = args.next() {
						arg.parse().map_err(Error::ParseIntError)?
					} else {
						return Err(Error::ExpectedInt);
					};
				}
				"-o" => {
					conf.file_name = if let Some(arg) = args.next() {
						arg
					} else {
						return Err(Error::ExpectedFileName);
					};
				}
				"-j" | "--threads" => {
					conf.thread_count = if let Some(arg) = args.next() {
						arg.parse().map_err(Error::ParseIntError)?
					} else {
						return Err(Error::ExpectedInt);
					};
				}
				"-h" | "--help" => {
					println!("{}", HELP_MESSAGE);
					std::process::exit(0);
				}
				_ => return Err(Error::InvalidParam(arg.clone())),
			}
		}
		Ok(conf)
	}
}

impl Default for Config {
	fn default() -> Config {
		Config {
			width: 800,
			height: 450,
			fps: 30,
			period: 256,
			ellipse_thickness: 4.0,
			ellipse_count: 25,
			file_name: "animat.gif".to_owned(),
			// available_parallelism() or default to 1
			thread_count: std::thread::available_parallelism()
				.map(|x| x.get())
				.unwrap_or(1),
		}
	}
}
