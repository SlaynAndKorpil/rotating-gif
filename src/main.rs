use std::fs::File;
use std::io::prelude::Write;
use std::thread;

mod config;
use config::*;

mod image;
use image::create_image;

/// The color index in the global color table to get the color black.
const BLACK_INDEX: u8 = 0;
/// The color index in the global color table to get the color white.
const WHITE_INDEX: u8 = 1;

/// The minimum code size, resulting from only using two colors.
const LZW_MINIMUM_CODE_SIZE: u8 = 2;

/// The maximum amount of bytes per sub block according to
/// [gif-spec89a.txt](https://www.w3.org/Graphics/GIF/spec-gif89a.txt).
const MAX_SUB_BLOCK_SIZE: usize = 255;

/// Terminator for sub blocks according to
/// [gif-spec89a.txt](https://www.w3.org/Graphics/GIF/spec-gif89a.txt).
const BLOCK_TERMINATOR: u8 = 0;

/// A byte introducing either an application or a graphic control extension.
///
/// Defined in
/// [gif-spec89a.txt](https://www.w3.org/Graphics/GIF/spec-gif89a.txt).
const EXTENSION_INTRODUCER: u8 = 0x21;

const HELP_MESSAGE: &str = r#"Usage: rotating-gif <options>

  The generated images can be influenced by the following options:
    -x, --width [u16]     Image width in pixels (Default: 800)
    -y, --height [u16]    Image height in pixels (Default: 450)
    -t, --tickness [f32]  Thickness of the ellipses in pixels (Default: 4.0)
    -c, --count [usize]   Number of ellipses (Default: 25)

  Options to influence the gif generation:
    -f, --fps [usize]     Frames displayed per second (Default: 30)
    -p, --period [usize]  Number of images to create (Default: 256)

  Other options:
    -o [filename]         Store the gif in this file (Default: "animat.gif") 
    -j, --threads [usize] Number of threads to use (Determines the number of available
                          threads with std::thread::available_parallelism() as default)
    -h, --help            Show this help message
"#;

/// Get the program args and generate a gif file.
fn main() -> std::io::Result<()> {
	let conf = Config::new().unwrap();
	make_gif(&conf)
}

/// Makes a gif file
fn make_gif(config: &Config) -> std::io::Result<()> {
	// GIF file format (taken from gif-spec89a.txt):
	//
	//
	// <GIF Data Stream> ::=     Header <Logical Screen> <Data>* Trailer
	//
	// <Logical Screen> ::=      Logical Screen Descriptor [Global Color Table]
	//
	// <Data> ::=                <Graphic Block>  |
	//                           <Special-Purpose Block>
	//
	// <Graphic Block> ::= [Graphic Control Extension] <Graphic-Rendering Block>
	//
	// <Graphic-Rendering Block> ::=  <Table-Based Image>  |
	//                                Plain Text Extension
	//
	// <Table-Based Image> ::=   Image Descriptor [Local Color Table] Image Data
	//
	// <Special-Purpose Block> ::=    Application Extension  |
	//                                Comment Extension
	//
	// see also: https://en.wikipedia.org/wiki/GIF#File_format

	let mut file = File::create(&config.file_name)?;

	// A buffer for stuff to write to the file
	let mut contents = Vec::new();

	// header
	contents.extend_from_slice(b"GIF89a");

	// width and height as byte slices
	let width_bytes = config.width.to_le_bytes();
	let height_bytes = config.height.to_le_bytes();

	let logical_screen_descriptor: [u8; 7] = [
		width_bytes[0],
		width_bytes[1],
		height_bytes[0],
		height_bytes[1],
		// packed fields:
		// global color flag set, color resolution of 1, not sorted, size of global
		// color table is 4
		0x80,
		// background color index
		WHITE_INDEX,
		// pixel aspect ratio
		0x00,
	];
	contents.extend_from_slice(&logical_screen_descriptor);

	// 3-byte RGB color table with 2 entries: black and white
	contents.extend_from_slice(&[0x00; 3]);
	contents.extend_from_slice(&[0xFF; 3]);

	let application_identifier: &[u8; 8] = b"NETSCAPE";
	let application_authentication_code: &[u8; 3] = b"2.0";
	// indicates the number of repetitions, 0 means infinite
	let repetition_count: [u8; 2] = 0u16.to_le_bytes();

	// NETSCAPE application extension for gif repetition
	let application_extension = [
		EXTENSION_INTRODUCER,
		// extension label
		0xFF,
		// size of this block in bytes
		(application_identifier.len() + application_authentication_code.len()) as u8,
		application_identifier[0],
		application_identifier[1],
		application_identifier[2],
		application_identifier[3],
		application_identifier[4],
		application_identifier[5],
		application_identifier[6],
		application_identifier[7],
		application_authentication_code[0],
		application_authentication_code[1],
		application_authentication_code[2],
		// application data size
		0x03,
		// application data block index
		0x01,
		repetition_count[0],
		repetition_count[1],
		BLOCK_TERMINATOR,
	];
	contents.extend_from_slice(&application_extension);

	// the time between every frame, in 1/100s
	let delay_time = (100 / config.fps).to_le_bytes();

	let graphic_control_extension: [u8; 8] = [
		EXTENSION_INTRODUCER,
		// graphic control label
		0xF9,
		// block size
		0x04,
		// packed fields:
		// disposal unspecified, no user input, no transparent color
		0x00,
		delay_time[0],
		delay_time[1],
		// transparent color index
		0x02,
		BLOCK_TERMINATOR,
	];
	contents.extend_from_slice(&graphic_control_extension);

	file.write_all(&contents)?;

	let image_descriptor: [u8; 10] = [
		// image separator
		0x2C,
		// image offset from left (16 bit)
		0x00,
		0x00,
		// image offset from top (16 bit)
		0x00,
		0x00,
		// image width (16 bit)
		width_bytes[0],
		width_bytes[1],
		// image height (16 bit)
		height_bytes[0],
		height_bytes[1],
		// packed fields
		0x00,
	];

	// every thread calculates at most this amount of frames
	let chunk_size = (config.period as f32 / config.thread_count as f32).ceil() as usize;
	let mut thread_pool = Vec::with_capacity(config.thread_count);

	// spawn threads to create and compress the images
	for thread_idx in 0..config.thread_count {
		let config = config.clone();
		let min = thread_idx * chunk_size;
		// all threads but the last calculate `chunk_size` frames, the last
		// calculates only the remaining frames
		let max = (min + chunk_size).min(config.period);
		thread_pool.push(
			thread::Builder::new()
						.name(format!("test: thread {}", thread_idx))
						// using a higher stack size for big images
						// since the thread would crash otherwise
						.spawn(move || {
							let mut res = Vec::with_capacity(chunk_size);
							for i in min..max {
								let inst = std::time::Instant::now();
								res.push(create_image(&config, i));
								println!(
									"[{:3}]: created image {:4} in {:6.3}s",
									thread_idx,
									i,
									inst.elapsed().as_secs_f64());
							}
							res
						}).unwrap(),
		);
	}

	for thread in thread_pool {
		for image in thread.join().unwrap() {
			file.write_all(&image_descriptor)?;
			file.write_all(&[LZW_MINIMUM_CODE_SIZE])?;

			// split the image in sub blocks
			// every sub block starts with its length and then up to MAX_SUB_BLOCK_SIZE
			// bytes
			let mut i = 0;
			while i < image.len() {
				let block_size = std::cmp::min(MAX_SUB_BLOCK_SIZE, image.len() - i);

				file.write_all(&[block_size as u8])?;
				file.write_all(&image[i..i + block_size])?;

				i += MAX_SUB_BLOCK_SIZE;
			}

			file.write_all(&[BLOCK_TERMINATOR])?;
		}
	}

	// trailer
	file.write_all(b";")?;

	Ok(())
}
