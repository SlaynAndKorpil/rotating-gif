# rotating-gif

## Description

Produces an animated gif of a rotating cone consisting of multiple circles.

Generates the images and compresses and stores them in gif format without the use of any libraries (expect rust std).

## Installation

Requirements:

- [Rust compiler](https://www.rust-lang.org/tools/install)

Then compile with:

```bash
cargo build --release
```

The resulting binary is then located in `target/release/rotating-gif`

## Usage

```
Usage: rotating-gif <options>

  The generated images can be influenced by the following options:
    -x, --width [u16]     Image width in pixels (Default: 800)
    -y, --height [u16]    Image height in pixels (Default: 450)
    -t, --tickness [f32]  Thickness of the ellipses in pixels (Default: 4.0)
    -c, --count [usize]   Number of ellipses (Default: 25)

  Options to influence the gif generation:
    -f, --fps [usize]     Frames displayed per second (Default: 30)
    -p, --period [usize]  Number of images to create (Default: 256)

  Other options:
    -o [filename]         Store the gif in this file (Default: "animat.gif") -j,
    --threads [usize]     Number of threads to use (Determines the number of available
                          threads with std::thread::available_parallelism() as default)
    -h, --help            Show this help message
```

## Example

![the_result](https://gitlab.com/SlaynAndKorpil/rotating-gif/-/raw/master/animat.gif)
